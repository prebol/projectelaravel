<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmpresesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('empreses')->insert([
            'nom' => 'VideoJocs 1',
            'adreca' => 'Carrer Cosa 1',
            'telefon' => '123456789',
            'correu' => 'video@video.cat',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('empreses')->insert([
            'nom' => 'VideoJocs 2',
            'adreca' => 'Carrer EmpresaController 3',
            'telefon' => '987654321',
            'correu' => 'video@video2.cat',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('empreses')->insert([
            'nom' => 'ERPs 1',
            'adreca' => 'Avinguda Backend 5',
            'telefon' => '112',
            'correu' => 'rrhh@erp.cat',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
