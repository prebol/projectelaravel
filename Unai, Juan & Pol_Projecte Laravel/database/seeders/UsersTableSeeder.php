<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //name, email, password, coordinador, grup, email_verified_at
        DB::table('users')->insert([
            'name' => 'tutor1',
            'password' => bcrypt('super3'),
            'email' => 'tutor1@carpediem.net',
            'grup'=>'tutor',
            'curs'=>1,
            'email_verified_at' => now(),
            'created_at' => now(),
        ]);
        DB::table('users')->insert([
            'name' => 'tutor2',
            'password' => bcrypt('super3'),
            'email' => 'tutor2@carpediem.net',
            'grup'=>'tutor',
            'curs'=>1,
            'email_verified_at' => now(),
            'created_at' => now(),
        ]);
        DB::table('users')->insert([
            'name' => 'tutor3',
            'password' => bcrypt('super3'),
            'email' => 'tutor3@carpediem.net',
            'grup'=>'tutor',
            'curs'=>1,
            'email_verified_at' => now(),
            'created_at' => now(),
        ]);
        DB::table('users')->insert([
            'name' => 'Coordinador1',
            'password' => bcrypt('super3'),
            'email' => 'coordinadorfct@carpediem.net',
            'grup'=>'tutor',
            'curs'=>2,
            'email_verified_at' => now(),
            'created_at' => now(),
        ]);
        DB::table('users')->insert([
            'name' => 'UnaiTutor',
            'password' => bcrypt('super3'),
            'email' => 'utut@carpediem.net',
            'grup'=>'tutor',
            'curs'=>2,
            'email_verified_at' => now(),
            'created_at' => now(),
        ]);
        DB::table('users')->insert([
            'name' => 'UnaiCoord',
            'password' => bcrypt('super3'),
            'email' => 'ucoord@carpediem.net',
            'grup'=>'coord',
            'curs'=>2,
            'email_verified_at' => now(),
            'created_at' => now(),
        ]);
    }
}
