<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OfertesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //descripcio, curs, nomcontacte, cognomscontacte, correucontacte, idEstudi, idEmpresa
        DB::table('ofertes')->insert([
            'descripcio' => 'Oferta 1',
            'curs' => 2022,
            'nomcontacte' => "Contacte 1",
            'cognomscontacte' => "Cognoms Contacte 1",
            'correucontacte' => 'contacte1@video.cat',
            'numVacants' => 4,
            'idEmpresa' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('ofertes')->insert([
            'descripcio' => 'Oferta 2',
            'curs' => 2022,
            'nomcontacte' => "Contacte 2",
            'cognomscontacte' => "Cognoms Contacte 2",
            'correucontacte' => 'contacte2@video2.cat',
            'numVacants' => 1,
            'idEmpresa' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('ofertes')->insert([
            'descripcio' => 'Oferta 3',
            'curs' => 2022,
            'nomcontacte' => "Contacte 3",
            'cognomscontacte' => "Cognoms Contacte 3",
            'correucontacte' => 'contacte1@video.cat',
            'numVacants' => 4,
            'idEmpresa' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('ofertes')->insert([
            'descripcio' => 'Oferta 4',
            'curs' => 2022,
            'nomcontacte' => "Contacte 4",
            'cognomscontacte' => "Cognoms Contacte 4",
            'correucontacte' => 'contacte1@erp.cat',
            'numVacants' => 4,
            'idEmpresa' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
