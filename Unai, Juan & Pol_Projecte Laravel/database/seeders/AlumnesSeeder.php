<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AlumnesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('alumnes')->insert([
            'nom' => 'Nom1',
            'cognoms' => 'Cognoms1',
            'DNI' => '11111111Z',
            'curs' => 2022,
            'telefon' => '088',
            'correu' => 'alumne1@ies.cat',
            'idUser' => 1,
            'curs' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('alumnes')->insert([
            'nom' => 'Nom2',
            'cognoms' => 'Cognoms2',
            'DNI' => '11111112T',
            'curs' => 2022,
            'telefon' => '931112233',
            'correu' => 'alumne2@ies.cat',
            'idUser' => 2,
            'curs' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('alumnes')->insert([
            'nom' => 'Nom3',
            'cognoms' => 'Cognoms3',
            'DNI' => '11111113P',
            'curs' => 2022,
            'telefon' => '935552233',
            'correu' => 'alumne2@ies.cat',
            'idUser' => 1,
            'curs' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('alumnes')->insert([
            'nom' => 'Nom4',
            'cognoms' => 'Cognoms4',
            'DNI' => '11111114X',
            'curs' => 2022,
            'telefon' => '937772233',
            'correu' => 'alumne2@ies.cat',
            'idUser' => 1,
            'curs' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        //nom, cognoms, DNI, correu, telefon, curs, idUser, idEstudi

    }
}
