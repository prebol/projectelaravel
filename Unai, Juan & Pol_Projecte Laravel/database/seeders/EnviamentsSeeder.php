<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EnviamentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //estat, observacions, idOferta, idAlumne
        DB::table('enviaments')->insert([
            'estat' => 'Acceptat',
            'idAlumne' => 1,
            'idOferta' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('enviaments')->insert([
            'estat' => 'Acceptat',
            'idAlumne' => 2,
            'idOferta' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('enviaments')->insert([
            'estat' => 'Acceptat',
            'idAlumne' => 1,
            'idOferta' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('enviaments')->insert([
            'estat' => 'Acceptat',
            'idAlumne' => 1,
            'idOferta' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
