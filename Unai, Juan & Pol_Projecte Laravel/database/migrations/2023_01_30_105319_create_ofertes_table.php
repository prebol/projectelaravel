<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('ofertes', function (Blueprint $table) {
            $table->bigIncrements("idOferta");
            $table->text("descripcio");
            $table->integer("curs");
            $table->string("nomcontacte",100);
            $table->string("cognomscontacte",200);
            $table->string("correucontacte",20);
            $table->integer("numVacants");
            $table->foreignId('idEmpresa')->nullable()->constrained('empreses')->references('idEmpresa');
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ofertes', function (Blueprint $table) {
            $table->dropForeign(['ofertes_idEstudi_foreign']);
            $table->dropColumn('idEstudi');
            $table->dropForeign(['ofertes_idEmpresa_foreign']);
            $table->dropColumn('idEmpresa');
        });
        Schema::dropIfExists('ofertes');
    }
};
