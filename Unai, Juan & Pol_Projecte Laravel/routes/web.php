<?php

use App\Http\Controllers\ControllerApp;
use App\Http\Controllers\EmpresaController;
use App\Http\Controllers\OfertesController;
use App\Http\Controllers\AlumnesController;
use App\Http\Controllers\EnviamentsController;
use App\Http\Controllers\EstudisController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/
Auth::routes();


Route::get('/getViewAllUsers',[ControllerApp::class,'getAllUsers']);
Route::get('/empresa',[EmpresaController::class,'getAllEmpreses'])->name('empresa')->middleware('auth');

Route::get('/getViewAllEstudis',[EstudisController::class,'getAllEstudis']);

Route::get('/alumnes',[AlumnesController::class,'getAllAlumnes'])->name('alumnes')->middleware('auth');
Route::get('/empresa/oferta',[OfertesController::class,'getAllOfertes'])->name('oferta')->middleware('auth');
Route::get('/empresa/oferta/add/{idEmpresa}',[OfertesController::class,'novaOferta'])->name('novaOferta')->middleware('auth');

Route::get('/getViewAllEnviaments',[EnviamentsController::class,'getAllEnviaments'])->name('enviaments');

Route::get('/novaEmpresa', [\App\Http\Controllers\EmpresaController::class, 'novaEmpresa'])->name('novaEmpresa')->middleware('auth');
Route::post('/novaEmpresa',[\App\Http\Controllers\EmpresaController::class, 'addEmpresa'])->name('addEmpresa')->middleware('auth');

Route::get('/editEmpresa/{id}', [\App\Http\Controllers\EmpresaController::class, 'editEmpresa'])->name('editEmpresa')->middleware('auth');
Route::post('/editEmpresa',[\App\Http\Controllers\EmpresaController::class, 'guardarEmpresa'])->name('guardarEmpresa')->middleware('auth');

Route::post('/guardaAlumne/{id}',[\App\Http\Controllers\AlumnesController::class, 'guardaAlumne'])->name('guardaAlumne')->middleware('auth');

Route::get('/editAlumne/{id}', [\App\Http\Controllers\AlumnesController::class, 'editAlumne'])->name('editAlumne')->middleware('auth');
//Route::post('/editAlumne',[\App\Http\Controllers\AlumnesController::class, 'guardarAlumne'])->name('guardarAlumne')->middleware('auth');

Route::get('/nouAlumne', [\App\Http\Controllers\AlumnesController::class, 'nouAlumne'])->name('nouAlumne')->middleware('auth');
Route::post('/nouAlumne',[\App\Http\Controllers\AlumnesController::class, 'addAlumne'])->name('addAlumne')->middleware('auth');

Route::post('/novaOferta',[\App\Http\Controllers\OfertesController::class, 'addOferta'])->name('addOferta')->middleware('auth');
Route::get('/empresa/tutor/oferta/{idOferta}/{numvacants}',[\App\Http\Controllers\OfertesController::class, 'restarvacantes'])->name('restarVacantes')->middleware('auth');

Route::get('/setOfertaAlumne/{idOferta}/{idAlumne}',[OfertesController::class,'setOfertaAlumne']);

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
