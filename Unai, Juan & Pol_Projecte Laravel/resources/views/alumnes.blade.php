<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <strong>{{ $message }}</strong>

            </div>
        @endif
        @if($user->grup=="tutor")
        <div class="card">
            <div class="card-header">{{ __('Alumnes') }}</div>


            <div class="card-body">

                <table class="table table-striped table-hover mb-5">
                    <thead>
                    <tr>
                        <th>ID Alumne</th>
                        <th>Nom</th>
                        <th>Cognom</th>
                        <th>DNI</th>
                        <th>Correu</th>
                        <th>Telèfon</th>
                        <th>curs</th>
                        <th>ID User</th>
                        <th>ID Estudi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($alumnes as $alumne)
                        @if($alumne->idUser==$user->id)
                        <tr id="{{$alumne->idAlumne}}">
                            <td> {{$alumne->idAlumne}} </td>
                            <td> {{$alumne->nom}} </td>
                            <td> {{$alumne->cognoms}} </td>
                            <td> {{$alumne->DNI}} </td>
                            <td> {{$alumne->correu}} </td>
                            <td> {{$alumne->telefon}} </td>
                            <td> {{$alumne->curs}} </td>
                            <td> {{$alumne->idUser}} </td>
                            <td> {{$alumne->idEstudi}} </td>


                            <td>
                                <a title="Editar Alumne" class="btn btn-primary" href="{{ route('editAlumne',[$alumne->idAlumne]) }}">

                                    Editar

                                </a>
                            </td>

                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>


                <a title="Afegir Alumne" class="btn btn-primary" href="{{ route('nouAlumne')}}">

                    Nou Alumne

                </a>


            </div>
        </div>
        @endif
    </div>
@endsection

</body>
</html>
