<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <strong>{{ $message }}</strong>
            </div>
        @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
        <div class="card">
            <div class="card-header">{{ __('Empreses') }}</div>

            <div class="card-body">

                <table class="table table-striped table-hover mb-5">
                    <thead>
                    <tr>
                        <th>ID Oferta</th>
                        <th>Descripcio</th>
                        <th>Curs</th>
                        <th>Nom Contacte</th>
                        <th>Cognom Contacte</th>
                        <th>Correu</th>
                        <th>Nombre Vacants</th>
                        <th>ID Empresa</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($ofertes as $oferta)
                        <tr id="{{$oferta->idOferta}}">
                            <td> {{$oferta->idOferta}} </td>
                            <td> {{$oferta->descripcio}} </td>
                            <td> {{$oferta->curs}} </td>
                            <td> {{$oferta->nomcontacte}} </td>
                            <td> {{$oferta->cognomscontacte}} </td>
                            <td> {{$oferta->correucontacte}} </td>
                            <td> {{$oferta->numVacants}} </td>
                            <td> {{$oferta->idEmpresa}} </td>
                            <td>
                                <a title="Editar Oferta" class="btn btn-primary" href="{{ route('editEmpresa',[$oferta->idOferta]) }}">

                                    Editar

                                </a>
                            </td>
                            <td>
                                <a title="Restar 1 Vacante" class="btn btn-primary" href="{{ route('restarVacantes',[$oferta->idOferta,1]) }}">

                                    Restar Vacante

                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <a title="Afegir Empresa" class="btn btn-primary" href="{{ route('novaEmpresa')}}">

                    Nova Empresa

                </a>

            </div>
        </div>
    </div>
@endsection

</body>
</html>
