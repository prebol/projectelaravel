<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Editar Alumne: {{ $alumne->nom }}</div>
            <div class="card-body">
                <form method="POST" action="{{ route('guardaAlumne',$alumne->idAlumne) }}" enctype="multipart/form-data">
                    @csrf
                    <input id="nom" type="text" name="idAlumne" value="{{ $alumne->idAlumne }}" hidden>
                    <div class="row mb-3">
                        <label for="nom" class="col-md-4 col-form-label text-md-end">Nom</label>

                        <div class="col-md-6">
                            <input id="nom" type="text" class="form-control @error('nom') is-invalid @enderror" name="nom" value="{{ $alumne->nom }}" required autocomplete="nom" autofocus placeholder="Escriu el nom">

                            @error('nom')
                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="cognoms" class="col-md-4 col-form-label text-md-end">Cognoms</label>

                        <div class="col-md-6">
                            <input id="cognoms" type="text" class="form-control @error('cognoms') is-invalid @enderror" name="cognoms" value="{{ $alumne->cognoms }}" required autocomplete="cognoms" autofocus placeholder="Escriu els cognoms">

                            @error('cognoms')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="DNI" class="col-md-4 col-form-label text-md-end">DNI</label>

                        <div class="col-md-6">
                            <input id="DNI" type="text" class="form-control @error('DNI') is-invalid @enderror" name="DNI" value="{{ $alumne->DNI }}" required autocomplete="DNI" autofocus placeholder="Escriu el DNI">

                            @error('DNI')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="correu" class="col-md-4 col-form-label text-md-end">{{ __('Correu') }}</label>

                        <div class="col-md-6">
                            <input id="correu" type="email" class="form-control @error('correu') is-invalid @enderror" name="correu" value="{{ $alumne->correu }}" required autocomplete="correu" placeholder="Escriu el correu">

                            @error('correu')
                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="telefon" class="col-md-4 col-form-label text-md-end">{{ __('Telèfon') }}</label>

                        <div class="col-md-6">
                            <input id="telefon" type="text" class="form-control @error('telefon') is-invalid @enderror" name="telefon" value="{{ $alumne->telefon }}" required autocomplete="telefon" placeholder="Escriu el telèfon">

                            @error('telefon')
                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="curs" class="col-md-4 col-form-label text-md-end">{{ __('Curs') }}</label>

                        <div class="col-md-6">
                            <input id="curs" type="text" class="form-control @error('curs') is-invalid @enderror" name="curs" value="{{ $alumne->curs }}" required autocomplete="curs" placeholder="Escriu el curs">

                            @error('curs')
                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="idUser" class="col-md-4 col-form-label text-md-end">{{ __('idUser') }}</label>

                        <div class="col-md-6">
                            <input id="idUser" type="text" class="form-control @error('idUser') is-invalid @enderror" name="idUser" value="{{ $alumne->idUser }}" required autocomplete="idUser" placeholder="Escriu el idUser">

                            @error('idUser')
                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="idEstudi" class="col-md-4 col-form-label text-md-end">{{ __('idEstudi') }}</label>

                        <div class="col-md-6">
                            <input id="idEstudi" type="text" class="form-control @error('idEstudi') is-invalid @enderror" name="idEstudi" value="{{ $alumne->idEstudi }}" required autocomplete="idEstudi" placeholder="Escriu el idEstudi">

                            @error('idEstudi')
                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                Guardar
                            </button>
                            <a title="Cancelar" class="btn btn-primary" href="{{ route('alumnes')}}">
                                Cancel·lar
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

</body>
</html>
