<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Editar Empresa: {{ $empresa->nom }}</div>
            <div class="card-body">
                <form method="POST" action="{{ route('guardarEmpresa',$empresa->idEmpresa) }}" enctype="multipart/form-data">
                    @csrf
                    <input id="nom" type="text" name="idEmpresa" value="{{ $empresa->idEmpresa }}" hidden>
                    <div class="row mb-3">
                        <label for="nom" class="col-md-4 col-form-label text-md-end">Nom</label>

                        <div class="col-md-6">
                            <input id="nom" type="text" class="form-control @error('nom') is-invalid @enderror" name="nom" value="{{ $empresa->nom }}" required autocomplete="nom" autofocus placeholder="Escriu el nom">

                            @error('nom')
                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="adreca" class="col-md-4 col-form-label text-md-end">Adreca</label>

                        <div class="col-md-6">
                            <input id="adreca" type="text" class="form-control @error('adreca') is-invalid @enderror" name="adreca" value="{{ $empresa->adreca }}" required autocomplete="adreca" autofocus placeholder="Escriu l'adreça">

                            @error('adreca')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="telefon" class="col-md-4 col-form-label text-md-end">Telèfon</label>

                        <div class="col-md-6">
                            <input id="telefon" type="text" class="form-control @error('telefon') is-invalid @enderror" name="telefon" value="{{ $empresa->telefon }}" required autocomplete="telefon" autofocus placeholder="Escriu el telèfon">

                            @error('telefon')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="correu" class="col-md-4 col-form-label text-md-end">{{ __('Correu') }}</label>

                        <div class="col-md-6">
                            <input id="correu" type="email" class="form-control @error('correu') is-invalid @enderror" name="correu" value="{{ $empresa->correu }}" required autocomplete="correu" placeholder="Escriu el correu">

                            @error('correu')
                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                Guardar
                            </button>
                            <a title="Cancelar" class="btn btn-primary" href="{{ route('empresa')}}">
                                Cancel·lar
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

</body>
</html>
