<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card-header">{{ __('Enviaments') }}</div>


        <div class="card-body">

            <table class="table table-striped table-hover mb-5">
                <thead>
                <tr>
                    <th>ID Enviament</th>
                    <th>Nom Oferta</th>
                    <th>Nom Alumne i Cognoms</th>
                    <th>Estat</th>
                </tr>
                </thead>
                <tbody>
                @foreach($enviaments as $enviament)
                    <tr id="{{$enviament->idEnviament}}">
                        <td> {{$enviament->idEnviament}} </td>
                        <td> {{$enviament->descripcio}} </td>
                        <td> {{$enviament->nom}} {{$enviament->cognoms}} </td>
                        <td> {{$enviament->estat}} </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
            {{-- Pagination --}}
            <div class="d-flex justify-content-center">
                {{ $enviaments->links()}}
            </div>
        </div>
    </div>
@endsection

</body>
</html>
