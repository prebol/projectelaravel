<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-8">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>ERROR!</strong> T'has equivocat!!!
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">{{ __('Afegir empresa nova') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form  method="POST" action="/nouAlumne" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="nom">Nom</label>
                                    <input type="text" name="nom" class="form-control" id="nom" placeholder="Nom" required>
                                </div>
                                <div class="form-group">
                                    <label for="cognoms">Cognoms</label>
                                    <input type="text" name="cognoms" class="form-control" id="cognoms" placeholder="cognoms" required>
                                </div>
                                <div class="form-group">
                                    <label for="DNI">DNI</label>
                                    <input type="text" name="DNI" class="form-control" id="DNI" placeholder="DNI" required>
                                </div>
                                <div class="form-group mt-4">
                                    <label for="correu">Correu</label>
                                    <input type="email" name="correu" class="form-control" id="correu" aria-describedby="emailHelp" placeholder="Correu" required>
                                </div>
                                <div class="form-group mt-4">
                                    <label for="telefon">Telefon</label>
                                    <input type="text" name="telefon" class="form-control" id="telefon" placeholder="telefon" required>
                                </div>
                                <div class="form-group mt-4">
                                    <label for="curs">Curs</label>
                                    <input type="text" name="curs" class="form-control" id="curs" placeholder="curs">
                                </div>
                                <div class="form-group mt-4">
                                    <label for="idEstudi">ID Estudi</label>
                                    <input type="text" name="idEstudi" class="form-control" id="idEstudi" placeholder="idEstudi" required>
                                </div>
                                <div class="form-group">
                                    <label>
                                        Nuevo Archivo
                                    </label>
                                    <div>
                                        <input type="file" name="file" >
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary mt-4">Guardar</button>
                            <a title="Cancelar" class="btn btn-primary mt-4" href="/alumnes">

                                Cancel·lar

                            </a>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
</body>
</html>
