<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alumnes extends Model
{
    use HasFactory;
    protected $table = "alumnes";
    protected $primaryKey = "idAlumne";
    protected $fillable = ["idAlumne","nom", "cognoms", "DNI", "correu", "telefon", "curs", "idUser", "idEstudi","fitxer_cv"];

    public function users(){
        return $this->BelongsTo(User::class, 'id', 'id');
    }
    public function estudis(){
        return $this->BelongsTo(Empreses::class, 'idEstudi', 'idEstudi');
    }

    public function ofertes(){
        return $this->belongsToMany(Ofertes::class, "enviaments","idOferta","idAlumne")->withTimestamps();
    }
}
