<?php

namespace App\Http\Controllers;

use App\Models\Alumnes;
use App\Models\User;
use App\Rules\carpediem;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PhpParser\Node\Scalar\String_;

class AlumnesController extends Controller
{
    public function nouAlumne() {
        return view('nouAlumne');
    }


    public function addAlumne(Request $request) {
        $user = User::findOrFail(Auth::user()->id);
        $alumne = new Alumnes();
        $file = $request->file('file');
        $extension = $file->getClientOriginalName();



        $alumne = new Alumnes();
        $alumne->fitxer_cv=$extension;
        $alumne->nom = $request->input('nom');
        $alumne->cognoms = $request->input('cognoms');
        $alumne->DNI = $request->input('DNI');
        $alumne->correu = $request->input('correu');
        $alumne->telefon = $request->input('telefon');
        if($request->input('curs')!=null)
        {
            $alumne->curs = $request->input('curs');
        }
        else
        {
            $alumne->curs = $user->curs;
        }
        $alumne->idUser = $user->id;
        $alumne->idEstudi = $request->input('idEstudi');

        $alumne->save();

        return redirect('/alumnes')->with('success', "L'Alumne ".$alumne->nom." ha estat afegit correctament.");
    }

    public function editAlumne($id) {
        $alumne = Alumnes::findOrFail($id);
        return view('editAlumne',['alumne' => $alumne]);
    }

    public function guardaAlumne(Request $request) {
        $id = intval($request->input('idAlumne'));

        //$request = Route::current();
        $validation =  $this->validate($request, [
            'nom' => ['required', 'string', 'min:1', 'max:255'],
            'cognoms' => ['required', 'string', 'max:255'],
            'DNI' => ['required', 'string', 'max:255'],
            'correu' => ['required', 'string', 'email', 'min:5', 'max:100'], //Rule::unique('alumnes')->ignore($id,"idAlumne"),
            'telefon' => ['required', 'string', 'max:15'],
            'curs' => ['required', 'string', 'max:255'],
            'idUser' => ['required', 'string', 'max:255'],
            'idEstudi' => ['required', 'string', 'max:255'],
        ]);

        $alumne = Alumnes::findOrFail($id);


        $alumne->nom = $request->input('nom');
        $alumne->cognoms = $request->input('cognoms');
        $alumne->DNI = $request->input('DNI');
        $alumne->correu = $request->input('correu');
        $alumne->telefon = $request->input('telefon');
        $alumne->curs = $request->input('curs');
        $alumne->idUser = $request->input('idUser');
        $alumne->idEstudi = $request->input('idEstudi');

        $alumne->update();
        return redirect(route('alumnes'))->with('success', "L'alumne ".$alumne->nom." ha estat guardat correctament.");
    }

    public static function getAllAlumnes(){
        $alumnes = Alumnes::all();
        $user = User::findOrFail(Auth::user()->id);
        return view('alumnes', [
            'user' => $user,
            'alumnes' => $alumnes
        ]);
    }
}
