<?php

namespace App\Http\Controllers;

use App\Models\Enviaments;
use Illuminate\Http\Request;

class EnviamentsController extends Controller
{
    public function getAllEnviaments(){
        $tots = Enviaments::paginate(5);
        return $tots->toJson();
    }
}
