<?php

namespace App\Http\Controllers;

use App\Models\Alumnes;
use App\Models\Ofertes;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OfertesController extends Controller
{
    public function getAllOfertes(){
        /*$tots = Ofertes::all();
        return $tots->toJson();*/
        $user = User::findOrFail(Auth::user()->id);
        $ofertes = Ofertes::all();
        return view('oferta', [
            'user' => $user,
            'ofertes' => $ofertes
        ]);
    }
    public function novaOferta($id){
        $oferta = Ofertes::findOrFail($id);
        return view('novaOferta',['oferta' => $oferta]);
    }
    public function restarvacantes($id,$n){
        $oferta = Ofertes::findOrFail($id);
        if($oferta->numVacants>0){

            $oferta->numVacants=$oferta->numVacants-$n;
            $oferta->update();
            return redirect(route('oferta'))->with('success', "Se ha restado una vacante a la oferta de: ".$oferta->nomcontacte);
        }
        else{
            return redirect(route('oferta'))->with('error', "L'Oferta de: ".$oferta->nomcontacte." no tiene vacantes disponibles.");
        }
    }
    public function addOferta(Request $request) {

        $oferta = new Ofertes();
        $oferta->descripcio = $request->input('desc');
        $oferta->curs = $request->input('curs');
        $oferta->nomcontacte = $request->input('nom');
        $oferta->cognomscontacte = $request->input('cognom');
        $oferta->correucontacte = $request->input('correu');
        $oferta->idEstudi = $request->input('Estudi');
        $oferta->idEmpresa = $request->input('EmpresaController');
        $oferta->save();

        return redirect(route('empresa'))->with('success', "L'Oferta de: ".$oferta->nomcontacte." ha estat afegida correctament.");
    }
    public function setOfertaAlumne($idOferta,$idAlumne){
        $oferta = Ofertes::find($idOferta);
        $alumne = Alumnes::find($idAlumne);
        $estat = "Acceptat";
        $alumne->ofertes()->attach($oferta, ["estat"=>$estat]);
        $alumne2 = Alumnes::find($alumne->idAlumne);
        $ofertes = $alumne2->ofertes()->get();
        return $ofertes->toJson();

    }
}
